## Hbit
This is a full stack project that help people track their habits.
For example if you want to smoke, you will add it in your habit and what action you have taken.
Did you gave in and go for it? Or did you resist. 

## Motivation
Tracking your actions in order to create a positive habit or make a bad habit stop is not something new.
But we sometimes don't take action and those always won't be recorded. This project encourage people to add their motivation, craving and ... as well as their actions.

## Current status
Right now the project is simply in demo phase and many things should be added, such as:
* Adding pagination
* Adding user resource and authentication (login, register and ...)
* Some design improvement
* Adding test for back end
* Adding positive habit tracker
* Adding content pages like about and ...

## Tech/framework used
* MongoDB
* Express.js
* React
* Node.js


## Installation
* cd client/
* npm run dev
* cd server
* node index.js
* You need to setup mongoDB and run it on the default setup


