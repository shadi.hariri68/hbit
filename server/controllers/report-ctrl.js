const Report = require('../models/report-model');

createReport = (req, res) => {
    const body = req.body;

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a report',
        })
    }

    const report = new Report(body);

    if (!report) {
        return res.status(400).json({success: false, error: err})
    }

    report
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: report._id,
                message: 'Report created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Report not created!',
            })
        })
}

updateReport = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    Report.findOne({_id: req.params.id}, (err, report) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'Report not found!',
            })
        }
        report.content = body.content;
        report.action = body.action;
        report.updated_at = Date.now();
        report
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: report._id,
                    message: 'report updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'Report not updated!',
                })
            })
    })
}

deleteReport = async (req, res) => {
    await Report.findOneAndDelete({_id: req.params.id}, (err, report) => {
        if (err) {
            return res.status(400).json({success: false, error: err})
        }

        if (!report) {
            return res
                .status(404)
                .json({success: false, error: `Report not found`})
        }

        return res.status(200).json({success: true, data: report})
    }).catch(err => console.log(err))
}

getReportById = async (req, res) => {
    await Report.findOne({_id: req.params.id}, (err, report) => {
        if (err) {
            return res.status(400).json({success: false, error: err})
        }

        if (!report) {
            return res
                .status(404)
                .json({success: false, error: `Report not found`})
        }
        return res.status(200).json({success: true, data: report})
    }).catch(err => console.log(err))
}

getReports = async (req, res) => {
    await Report.find({}).sort({createdAt: -1}).exec((err, reports) => {
        if (err) {
            return res.status(400).json({success: false, error: err})
        }
        return res.status(200).json({success: true, data: reports})
    })
}

module.exports = {
    createReport,
    updateReport,
    deleteReport,
    getReports,
    getReportById,
}