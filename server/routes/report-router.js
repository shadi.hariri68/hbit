const express = require('express')

const ReportCtrl = require('../controllers/report-ctrl');

const router = express.Router();

router.post('/reports', ReportCtrl.createReport);
router.put('/reports/:id', ReportCtrl.updateReport);
router.delete('/reports/:id', ReportCtrl.deleteReport);
router.get('/reports/:id', ReportCtrl.getReportById);
router.get('/reports', ReportCtrl.getReports);

module.exports = router