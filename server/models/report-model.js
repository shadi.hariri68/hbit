const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Report = new Schema(
    {
        content: {type: String, required: true},
        action: {type: String, required: true},
    },
    {timestamps: true},
)

module.exports = mongoose.model('reports', Report)
