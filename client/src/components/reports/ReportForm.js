import React, {Component} from 'react';
import PropTypes from 'prop-types';


class ReportForm extends Component {
    constructor(props) {
        super(props);
        if (props.report) {
            this.state = {
                _id: props.report._id,
                content: props.report.content,
                action: props.report.action,
            };
        } else {
            this.state = {
                content: '',
                action: 'Resist',
            };
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeInputContent = this.handleChangeInputContent.bind(this)
    }

    handleAccept = (event) => {
        event.preventDefault();
        this.props.submitReport(this.state);
    };
    handleCancel = (event) => {
        event.preventDefault();
        this.props.cancelReport();
    };

    handleChange(event) {
        this.setState({action: event.target.value});
    }

    handleChangeInputContent = async event => {
        const content = event.target.value
        this.setState({content: content})
    }

    render() {
        const {action, content} = this.state

        return (
            <div>
                <form className="row d-flex align-items-center justify-content-between" onSubmit={this.handleAccept}>
                    <div className="form-group flex-grow-1 px-3">
                        <small>Your habit: </small>
                        <input type="text" className="form-control" value={content}
                               placeholder="smoking a cigarette" onChange={this.handleChangeInputContent}/>
                    </div>
                    <div className="form-group px-1">
                        <small>Action: </small>
                        <select className="form-control" onChange={this.handleChange} value={action}>
                            <option value='Resist'>Resist</option>
                            <option value='Gave in'>Gave in</option>
                            <option value='Donated'>Donated</option>
                        </select>
                    </div>
                    <div className=' justify-content-end d-flex pt-2'>
                        <button className={'btn btn-primary btn-sm mr-1'} type='submit'>
                            {this.props.report ? 'Update' : 'Add'}
                        </button>
                        <button className={'btn btn-secondary btn-sm'} type={'button'} onClick={this.handleCancel}>
                            Cancel
                        </button>
                    </div>
                </form>
            </div>


        );
    }
}

// eslint-disable-next-line react/no-typos
ReportForm.propTypes = {
    report: PropTypes.object,
    submitReport: PropTypes.func.isRequired,
    cancelReport: PropTypes.func.isRequired


};
export default ReportForm;
