import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ConfirmModal from "../ConfirmModal";
import ReportForm from "./ReportForm";


class Report extends Component {
    state = {showModal: false, showForm: false,};

    showForm = () => {
        this.setState({showForm: true});
    };
    hideForm = () => {
        this.setState({showForm: false});
    };


    showModal = () => {
        this.setState({showModal: true});
    };
    hideModal = () => {
        this.setState({showModal: false});
    };


    handleDelete = () => {
        this.props.deleteReport();
        this.hideModal();
    };
    handleUpdate = (state) => {
        this.props.updateReport(state);
        this.hideForm();
    };

    render() {
        return (
            <div>
                <ConfirmModal show={this.state.showModal} message={'Are you sure you want to delete this report?'}
                              handleAccept={this.handleDelete} handleClose={this.hideModal}/>
                <div className={'mb-3 ' + (this.state.showForm ? 'fade-in d-block' : 'd-none')}>
                    <ReportForm report={this.props.report} cancelReport={this.hideForm} submitReport={this.handleUpdate}/>

                </div>
                <div
                    className={'list-group-item  justify-content-between align-items-center ' + (!this.state.showForm ? 'fade-in d-flex' : 'd-none')}>

                    <div>{this.props.report.content} <span className="badge badge-info">{this.props.report.action}</span></div>
                    <div className={'d-flex'}>
                        <button type="button" className="btn btn-outline-primary btn-sm mr-2" onClick={this.showForm}
                        >
                            Update
                        </button>
                        <button type="button" className=" btn-outline-danger btn-sm" onClick={this.showModal}
                        >
                            Delete
                        </button>
                    </div>
                </div>

            </div>


        );
    }
}

// eslint-disable-next-line react/no-typos
Report.propTypes = {
    report: PropTypes.object.isRequired,
    deleteReport: PropTypes.func.isRequired,
    updateReport: PropTypes.func.isRequired

};
export default Report;
