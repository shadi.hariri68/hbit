import React from 'react';
import PropTypes from 'prop-types';

import './scss/modal.css'

const ConfirmModal = ({handleClose, handleAccept, show, message}) => {
    const showHideClassName = show ? "modal d-block fade-in" : "d-none";

    return (
        <div>
            <div className={showHideClassName}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">

                        <div className="modal-body">
                            <div className={'text-center'}>

                                <p>{message}</p>
                                <button className={'btn btn-danger mr-2'} onClick={handleAccept}>Yes</button>
                                <button className={'btn btn-secondary'} onClick={handleClose}>Cancel</button>
                            </div>


                        </div>
                    </div>
                </div>

            </div>

        </div>

    );
};

// eslint-disable-next-line react/no-typos
ConfirmModal.propTypes = {
    message: PropTypes.string.isRequired
};
export default ConfirmModal;