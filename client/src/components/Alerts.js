import React from 'react';
import PropTypes from 'prop-types';
import './scss/alert.css'

const Alerts = ({message, type}) => {
    const typeClassName = type==='success' ? "alert-success" : (type==='error'? 'alert-danger':'alert-warning');

    return (
        <div className={'hbit-alert'}>
            <div className={'alert '+typeClassName}  role="alert">
                {message}
            </div>
        </div>

    );
};

// eslint-disable-next-line react/no-typos
Alerts.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired

};
export default Alerts;