import React, {Component} from 'react'
import Report from "../components/reports/Report";
import api from '../api'

import ReportForm from "../components/reports/ReportForm";
import Alerts from "../components/Alerts";


class ReportsList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reports: [],
            columns: [],
            showAddForm: false,
            alert: {
                type: '',
                message: ''
            }
        }
    }


    DeleteReport = (report) => {
        this.setState({isLoading: true});
        api.deleteReportById(report._id).then((result) => {
            this.setState({
                reports: this.state.reports.filter(r => {
                    return r._id !== result.data.data._id
                }),
            })
        }).catch((e) => {
            this.showError('An error occurred. Please try again.', 'error')

        }).finally(() => {
            this.setState({isLoading: false});
        });

    };

    UpdateReport = (report) => {
        this.setState({isLoading: true});
        let payload = {action: report.action, content: report.content}
        api.updateReportById(report._id, payload).then((result) => {
            let newReports = [];
            this.state.reports.forEach(r => {
                if (r._id === report._id) {
                    r = report;
                }
                newReports.push(r);
            });
            this.setState({
                reports: newReports,
            })
        }).catch((e) => {
            this.showError('An error occurred. Please try again.', 'error')

        }).finally(() => {
            this.setState({isLoading: false});

        });
    };


    AddReport = (report) => {
        this.setState({isLoading: true});
        let payload = {action: report.action, content: report.content}
        api.insertReport(payload).then((result) => {
            report._id = result.data.id;
            this.state.reports.push(report)
            this.setState({
                reports: this.state.reports,
            })
            this.hideAddForm();
        }).catch((e) => {
            this.showError('An error occurred. Please try again.', 'error')

        }).finally(() => {
            this.setState({isLoading: false});
        });

    };

    showAddForm = () => {
        this.setState({showAddForm: true});
    };
    hideAddForm = () => {
        this.setState({showAddForm: false});
    };

    showError = (message, type) => {
        this.setState({
            alert: {
                message: message,
                type: type
            }
        });
        setTimeout(() => {
            this.setState({
                alert: {
                    message: '',
                    type: ''
                }
            });
        }, 4000);
    };
    componentDidMount = async () => {
        this.setState({isLoading: true})

        await api.getAllReports().then(reports => {
            this.setState({
                reports: reports.data.data,
            })
        }).catch((e) => {
            this.showError('An error occurred. Please try again.', 'error')

        }).finally(() => {
            this.setState({isLoading: false});
        })
    };

    render() {
        const {reports} = this.state

        return (
            <div>
                    <div className={(this.state.alert.message !== '' ? 'fade-in ' : 'd-none')}>
                        <Alerts message={this.state.alert.message}
                                type={this.state.alert.type}/>
                    </div>
                <div className={"spinner-border text-info page-loader " + (!this.state.isLoading ? 'd-none' : '')}
                     role="status">
                    <span className="sr-only">Loading...</span>
                </div>
                <div className={'col-8 mx-auto ' + (this.state.showAddForm ? 'fade-in ' : 'd-none')}>
                    <ReportForm  cancelReport={this.hideAddForm}  submitReport={this.AddReport}/>
                </div>
                <div>
                    <div
                        className={'col-8 mx-auto justify-content-end mb-4 ' + (!this.state.showAddForm ? 'fade-in  d-flex' : 'd-none')}>
                        <button className={'btn btn-dark btn-sm'} onClick={this.showAddForm}>
                            Add new habit
                        </button>
                    </div>

                </div>
                <div className={'row'}>
                    <div className={'col-8 mx-auto'}>
                        <ul className={'list-group'}>
                            {
                                reports.map((report) => (
                                    <Report key={report._id} report={report}
                                            deleteReport={() => this.DeleteReport(report)}
                                            updateReport={this.UpdateReport}
                                    />
                                ))
                            }
                        </ul>
                    </div>
                </div>
                <div className={'row ' + (reports.length === 0 ? 'fade-in ' : 'd-none')}>

                    <div className={'col-8 mx-auto'}>
                        <ul className={'list-group'}>
                            <div className={'list-group-item  justify-content-between d-flex'}>
                                There is no habit submited yet.
                            </div>
                        </ul>
                    </div>
                </div>
            </div>


        )


    }
}

export default ReportsList