import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './pages/css/app.css'

ReactDOM.render(<App />, document.getElementById('root'));