import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import {NavBar} from '../components'
import {ReportsList} from '../pages'

import 'bootstrap/dist/css/bootstrap.min.css'
import Welcome from "../components/Welcome";

function App() {
    return (
        <Router>
            <NavBar/>
            <Welcome/>
            <Switch>
                <Route path="/" exact component={ReportsList}/>
            </Switch>
        </Router>
    )
}

export default App