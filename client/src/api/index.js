import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:8000/api',
})

export const insertReport = payload => api.post(`/reports`, payload)
export const getAllReports = () => api.get(`/reports`)
export const updateReportById = (id, payload) => api.put(`/reports/${id}`, payload)
export const deleteReportById = id => api.delete(`/reports/${id}`)
export const getReportById = id => api.get(`/reports/${id}`)

const apis = {
    insertReport,
    getAllReports,
    updateReportById,
    deleteReportById,
    getReportById,
}

export default apis